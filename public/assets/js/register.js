let registerUserForm = document.querySelector('#registerUser');

registerUserForm.addEventListener("submit", (e) => {
    
    e.preventDefault();

    let fName = document.querySelector('#firstName').value
    let lName = document.querySelector('#lastName').value
    let uEmail = document.querySelector('#userEmail').value
    let mNumber = document.querySelector('#mobileNumber').value
    let pw1 = document.querySelector('#password1').value
    let pw2 = document.querySelector('#password2').value

    // let password = document.querySelector("#password1").value
    // console.log(password)
    // let verifyPassword = document.querySelector("#password2").value
    // console.log(verifyPassword)

    if ((pw1 !== "" && pw2 !== "")&&(pw1 === pw2) && (mNumber.length === 11)) {

    	fetch('https://evening-island-69458.herokuapp.com/api/users/email-exists', {
            method: 'POST',
            headers:{
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: uEmail
            })

        }).then(res => res.json()
            ).then(data => {
                if(data === false){
                    fetch("https://evening-island-69458.herokuapp.com/api/users/register", {
                     method: 'POST',
                     headers: {
                         'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName: fName,
                lastName: lName,
                email: uEmail,
                mobileNo: mNumber,
                password: pw1
            })

        }).then(res => {return res.json()
            
        }).then(data => {
            console.log(data);
            if(data === true) {
                 Swal.fire({
                    icon:'success',
                    title: 'Welcome!',
                    text: 'Enter the Dojo!'
                });
            } else {
                Swal.fire({
                    icon:'error',
                    title: 'Pay Attention!',
                    text: 'You did something wrong!'
                });
            }
        })
        // alert("passwords met the criteria, create new document.")

                }else{
                  Swal.fire({
                    icon:'error',
                    title: 'Uh oh!',
                    text: 'email already exists, choose another email'
                });
                }
        })
    } else {
         Swal.fire({
            icon:'error', 
            title: 'Whoops!!',//the value of the title will be up to the dev.
            text: 'Finish the form first before clicking'//can be used to display/show more details/info about the action/response.
        });
    }
})