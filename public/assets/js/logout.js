//clear/wipeout all the data inside our local storage so that the session of the user will end.
localStorage.clear()
//the clear method will allow you to remove the contents of the storage object.

//redirect the user to the login page just in case a new user wants to log in.
window.location.replace('./login.html');