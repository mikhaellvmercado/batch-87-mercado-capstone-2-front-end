console.log('HI SCRIPT.JS')

let navItem = document.querySelector('#navSession')

//lets take the access token from the local storage property

let userToken = localStorage.getItem("token")


//lets create a control structure that will determine which elements inside the nav bar will be displayed if token is found in the local storage.
if(!userToken) {
	navItem.innerHTML = 
	`
	<li class="nav-item">
		<a href="./login.html" class="nav-link">Log in </a>
	</li>
	`
}else{
	navItem.innerHTML = 
	`
	<li class="nav-item">
		<a href="./logout.html" class="nav-link">Log Out</a>
	</li>
	`
}

