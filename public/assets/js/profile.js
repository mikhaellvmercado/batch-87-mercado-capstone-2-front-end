console.log('sUP');

let token = localStorage.getItem("token")
console.log(token)

let profile = document.querySelector('#profileContainer')

//lets create a control structure that will determine the display if the access token is null or empty.

if(!token || token === null ) {
	//lets redirect the user to the login page
	alert("You must Login First");
	window.location.href="./login.html"
}else{
	fetch('https://evening-island-69458.herokuapp.com/api/users/details', {
		method:'GET',
		headers:{
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	}).then(res => res.json())
	.then(data =>{
		console.log(data); //checking purposes
		profile.innerHTML =
		`				

		<div class="col-md-12">
				<section class="jumbotron my-5">
						<h3 class="text-center">First Name: ${data.firstName}</h3>
						<h3 class="text-center">Last Name: ${data.lastName}</h3>
						<h3 class="text-center">Email: ${data.email}</h3>			
						
						
						<table class="table">
								<thead>
										<tr>
												<th>Course ID:</th>
												<th>Enrolled On:</th>
												<th>Status:</th>
												<tbody></tbody>
										</tr>
								</thead>
						</table>						
				</section>
		</div>

		`
	})
}